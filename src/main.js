const { app, BrowserWindow } = require('electron');
const path = require('path');
const { readFile, rm, open } = require('fs/promises');

const MAX_STARTING_WIDTH = 1200;
const MAX_STARTING_HEIGHT = 800;

async function createWindow() {
    // We cannot require the screen module until the app is ready.
    const { screen } = require('electron');
    const primaryDisplay = screen.getPrimaryDisplay();
    const { width, height } = primaryDisplay.workAreaSize;

    const win = new BrowserWindow({
        width: (width > MAX_STARTING_WIDTH) ? MAX_STARTING_WIDTH : width,
        height: (height > MAX_STARTING_HEIGHT) ? MAX_STARTING_HEIGHT : height,
        fullscreenable: false,
        title: "Windows Electron Test",
    });

    const index_html = path.join(__dirname, './layout/index.html');
    win.loadFile(index_html);
    win.menuBarVisible = false;
}

app.whenReady().then(() => {
    createWindow().then(() => {
        app.on('activate', function () {
            if (BrowserWindow.getAllWindows().length === 0) createWindow();
        });
    });
});

app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') app.quit();
});
