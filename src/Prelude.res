open Belt

let uuid4: unit => string = %raw(`
  function () {
    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(
      /[018]/g,
      (c) => (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
  }
`)

let chooseFrom = choices => choices->Array.getUnsafe(Js.Math.random_int(0, choices->Array.length))

let then = (promise, function) => Js.Promise.then_(function, promise)
let catch = (promise, function) => Js.Promise.catch(function, promise)
let resolve = Js.Promise.resolve
let reject = Js.Promise.reject

let thenDo = (promise, function) => {
  promise
  ->then(x => {
    function(x)
    resolve(x)
  })
  ->ignore
}

/// Fork a promise, returning both the resolved value and the previous
/// promise.
let fork = (promise, function) => {
  (promise->then(function), promise)
}

/// Creates a promise that is resolved after some time (in milliseconds) have
/// passed.
let ellapsed = (milliseconds: int) =>
  Js.Promise.make((~resolve, ~reject) => {
    let nothing = ()
    Js.Global.setTimeout(() => resolve(. nothing)->ignore, milliseconds)->ignore
  })

// Array methods
@send external join: (array<'t>, string) => string = "join"

external toBool: 'a => bool = "%identity"

/**
 * A convience map with default for an `option<'a>` that returns a tuple of
 * `(bool, 'a)` where the first component is true when the optional value
 * wasn't None.
 */
let defaultWithBool = (value, default) => {
  value->Option.mapWithDefault((false, default), v => (true, v))
}

let maybe = Belt.Option.flatMap
let default = Belt.Option.getWithDefault

module DateTime = {
  let isBefore = (a: Js.Date.t, ref: Js.Date.t): bool => a->Js.Date.valueOf < ref->Js.Date.valueOf

  let isBeforeOrAt = (a: Js.Date.t, ref: Js.Date.t): bool =>
    a->Js.Date.valueOf <= ref->Js.Date.valueOf

  let isAfter = (a: Js.Date.t, ref: Js.Date.t): bool => a->Js.Date.valueOf > ref->Js.Date.valueOf

  let isAfterOrAt = (a: Js.Date.t, ref: Js.Date.t): bool =>
    a->Js.Date.valueOf >= ref->Js.Date.valueOf

}

module Date = {
  let _trimmed = a => Js.Date.makeWithYMD(
      ~year=a->Js.Date.getFullYear,
      ~month=a->Js.Date.getMonth,
      ~date=a->Js.Date.getDay,
      (),
    )

  let isBefore = (a: Js.Date.t, ref: Js.Date.t): bool => {
    let a = a->_trimmed;
    let ref = ref->_trimmed
    a->DateTime.isBefore(ref)
  }

  let isBeforeOrAt = (a: Js.Date.t, ref: Js.Date.t): bool => {
    let a = a->_trimmed;
    let ref = ref->_trimmed
    a->DateTime.isBeforeOrAt(ref)
  }

  let isAfter = (a: Js.Date.t, ref: Js.Date.t): bool => {
    let a = a->_trimmed;
    let ref = ref->_trimmed
    a->DateTime.isAfter(ref)
  }

  let isAfterOrAt = (a: Js.Date.t, ref: Js.Date.t): bool => {
    let a = a->_trimmed;
    let ref = ref->_trimmed
    a->DateTime.isAfterOrAt(ref)
  }
}
