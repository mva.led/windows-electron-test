open Belt
open Webapi

// bind to JS Array.prototype.join, otherwise we would have to do something like:
//
//   ->Belt.Array.reduce(" ", (a, b) => a ++ b)
//
@send external join: (array<'t>, string) => string = "join"

module Style = ReactDOMStyle


module Request = {
  type method = [#GET | #POST | #PUT | #PATCH | #DELETE | #HEAD]
  // type xhr

  type t = {
    url: string,
    method: method,
    params: Js.Dict.t<string>,
    body: Js.Dict.t<string>,
    headers: Js.Dict.t<string>,
    // config: xhr => xhr,
  }

  external webFormToJsDict: Webapi.FormData.t => Js.Dict.t<string> = "%identity"

  let get = url => {
    {
      url: url,
      method: #GET,
      params: Js.Dict.empty(),
      body: Js.Dict.empty(),
      headers: Js.Dict.empty(),
    }
  }

  let post = url => {
    {
      url: url,
      method: #POST,
      params: Js.Dict.empty(),
      body: Js.Dict.empty(),
      headers: Js.Dict.empty(),
    }
  }

  let payload = (request, payload) => {
    let payload = Js.Dict.fromArray(payload)
    switch request.method {
    | #GET | #HEAD => {...request, params: payload}
    | _ => {...request, body: payload}
    }
  }

  let form = (request, form) => {
    switch request.method {
    | #GET => {...request, params: form->webFormToJsDict}
    | _ => {...request, body: form->webFormToJsDict}
    }
  }

  // Create a new request with a new set of headers.
  //
  // The headers are given as an array of pairs `(header, value)`.  If the
  // same header appears more than once, the last value survices.
  let headers = (request: t, headers: Js.Array.t<(string, string)>) => {
    let headers = Js.Dict.fromArray(headers)
    {...request, headers: headers}
  }

  // Create a new request with extra headers.
  //
  // Unlike `headers`, we keep the current headers and override
  // them with `extraHeaders`.
  let addHeaders = (request, extraHeaders) => {
    let entries = request.headers->Js.Dict.entries->Js.Array.concat(extraHeaders)
    request->headers(entries)
  }

  @module("mithril") external do: t => Js.Promise.t<'a> = "request"
}

module Route = {
  type t
  @module("mithril") external _route: t = "route"

  // prefix
  @set external _setPrefix: (t, string) => unit = "prefix"
  let setPrefix = prefix => _route->_setPrefix(prefix)

  // set
  type options = {replace: bool}
  @module("mithril") @scope("route")
  external _set: (~url: string, ~params: Js.Dict.t<string>=?, ~options: options=?, unit) => unit =
    "set"
  let set = url => _set(~url, ())
  let setWithParams = (url, params) => _set(~url, ~params, ())
  let replace = url => _set(~url, ~options={replace: true}, ())
  let replaceWithParams = (url, params) => _set(~url, ~params, ~options={replace: true}, ())

  // get
  @module("mithril") @scope("route")
  external get: unit => option<string> = "get"
}

type element

@module("mithril") external mount: (Webapi.Dom.Element.t, 'a) => unit = "mount"
@module("mithril") external redraw: unit => unit = "redraw"
@module("mithril") external trust: string => element = "trust"

// State handling

type state<'state> = {mutable data: 'state}

type vnode<'attrs, 'state> = {
  attrs: 'attrs,
  @as("state") _state: state<'state>,
  dom: Webapi.Dom.Node.t,
  domSize: option<int>,
  children: array<element>,
}

let state = vnode => vnode._state.data
let setState = (vnode, state) => {
  vnode._state.data = state
  redraw()
}
let setStateWithoutRedraw = (vnode, state) => {
  vnode._state.data = state
}

// HTMLElement helpers

module Element = {
  let select = (vnode, selector) => {
    let dom = vnode.dom
    let length = Js.Math.max_int(vnode.domSize->Option.getWithDefault(1), 1)
    let (_, element) = Array.make(length, true)->Array.reduce((Some(dom), None), (
      (dom, item),
      _,
    ) => {
      switch (dom, item) {
      | (dom, Some(item)) => (dom, Some(item))
      | (Some(dom), None) => (
          dom->Dom.Node.nextSibling,
          switch dom->Dom.Node.nodeType {
          | Element => dom->Dom.Element.ofNode->Option.flatMap(Dom.Element.querySelector(selector))
          | _ => None
          },
        )
      | (None, None) => (None, None)
      }
    })
    element
  }

  let value = element => {
    element
    ->Option.flatMap(Dom.Element.asHtmlElement)
    ->Option.flatMap(element => {
      Some(element->Dom.HtmlElement.value)
    })
  }

  let focus = element => {
    element
    ->Option.flatMap(Dom.Element.asHtmlElement)
    ->Option.flatMap(element => {
      element->Dom.HtmlElement.focus
      None
    })
    ->ignore
  }
}

// Component definition

type component<'attrs, 'state> = {
  @as("data") state: 'state,
  oninit: option<vnode<'attrs, 'state> => unit>,
  oncreate: option<vnode<'attrs, 'state> => unit>,
  view: option<vnode<'attrs, 'state> => element>,
  onbeforeupdate: option<(vnode<'attrs, 'state>, vnode<'attrs, 'state>) => bool>,
  onupdate: option<vnode<'attrs, 'state> => unit>,
  onbeforeremove: option<vnode<'attrs, 'state> => Js.Promise.t<unit>>,
  onremove: option<vnode<'attrs, 'state> => unit>,
}

let component = state => {
  state: state,
  oninit: None,
  oncreate: None,
  view: None,
  onbeforeupdate: None,
  onupdate: None,
  onbeforeremove: None,
  onremove: None,
}
let oninit = (component, oninit) => {...component, oninit: Some(oninit)}
let oncreate = (component, oncreate) => {...component, oncreate: Some(oncreate)}
let view = (component, view) => {...component, view: Some(view)}
let onbeforeupdate = (component, onbeforeupdate) => {
  ...component,
  onbeforeupdate: Some(onbeforeupdate),
}
let onupdate = (component, onupdate) => {...component, onupdate: Some(onupdate)}
let onbeforeremove = (component, onbeforeremove) => {
  ...component,
  onbeforeremove: Some(onbeforeremove),
}
let onremove = (component, onremove) => {...component, onremove: Some(onremove)}

// Rendering helpers

@val external empty: element = "null"
external show: 'a => element = "%identity"

let class = classes => {
  classes
  ->Belt.Array.map(((class, expression)) =>
    if expression {
      class
    } else {
      ""
    }
  )
  ->Belt.Array.keep(class => class != "")
  ->join(" ")
}
