open Webapi
%%raw(`import mithril from "mithril";`)

// TODO: extend this domProps as we need from
// https://github.com/rescript-lang/rescript-react/blob/master/src/ReactDOM.res

type style = ReactDOMStyle.t

@deriving(abstract)
type domProps = {
  @optional
  key: string,
  @optional
  selector: string,
  // global html attributes
  @optional @as("class")
  className: string,
  @optional
  href: string,
  @optional @as("for")
  htmlFor: string /* substitute for "for" */,
  @optional
  id: string,
  @optional
  style: style,
  // tag-specific html attributes
  @optional
  accept: string,
  @optional @as("accesskey")
  accessKey: string,
  @optional
  action: string /* uri */,
  @optional
  alt: string,
  @optional
  async: bool,
  @optional @as("autocomplete")
  autoComplete: string /* has a fixed, but large-ish, set of possible values */,
  @optional @as("autocapitalize")
  autoCapitalize: string /* Mobile Safari specific */,
  @optional @as("autofocus")
  autoFocus: bool,
  @optional @as("autoplay")
  autoPlay: bool,
  @optional
  challenge: string,
  @optional
  charSet: string,
  @optional
  checked: bool,
  @optional
  cite: string /* uri */,
  @optional @as("crossorigin")
  crossOrigin: string /* anonymous, use-credentials */,
  @optional
  cols: int,
  @optional
  colSpan: int,
  @optional
  content: string,
  @optional
  controls: bool,
  @optional
  coords: string /* set of values specifying the coordinates of a region */,
  @optional
  data: array<(string, string)>,
  @optional
  dateTime: string /* "valid date string with optional time" */,
  @optional
  default: bool,
  @optional
  defer: bool,
  @optional
  disabled: bool,
  @optional
  download: string /* should really be either a boolean, signifying presence, or a string */,
  @optional
  encType: string /* "application/x-www-form-urlencoded", "multipart/form-data" or "text/plain" */,
  @optional
  headers: string,
  @optional
  height: string,
  @optional
  max: string /* should be int or Js.Date.t */,
  @optional
  name: string,
  @optional
  placeholder: string,
  @optional
  src: string /* uri */,
  @optional @as("type")
  type_: string /* has a fixed but large-ish set of possible values */ /* use this one. Previous one is deprecated */,
  /* Form events */
  @optional @as("oninput")
  onInput: Dom.InputEvent.t => unit,
  @optional @as("onsubmit")
  onSubmit: Dom.UiEvent.t => unit,
  // mouse events
  @optional @as("onclick")
  onClick: Dom.MouseEvent.t => unit,
}

@deriving(abstract)
type makeProps = {
  children: Mithril.element,
  @optional
  key: string,
  @optional
  selector: string,
  // global html attributes
  @optional @as("class")
  className: string,
  @optional
  href: string,
  @optional @as("for")
  htmlFor: string /* substitute for "for" */,
  @optional
  id: string,
  @optional
  style: style,
  // tag-specific html attributes
  @optional
  accept: string,
  @optional @as("accesskey")
  accessKey: string,
  @optional
  action: string /* uri */,
  @optional
  alt: string,
  @optional
  async: bool,
  @optional @as("autocomplete")
  autoComplete: string /* has a fixed, but large-ish, set of possible values */,
  @optional @as("autocapitalize")
  autoCapitalize: string /* Mobile Safari specific */,
  @optional @as("autofocus")
  autoFocus: bool,
  @optional @as("autoplay")
  autoPlay: bool,
  @optional
  challenge: string,
  @optional
  charSet: string,
  @optional
  checked: bool,
  @optional
  cite: string /* uri */,
  @optional @as("crossorigin")
  crossOrigin: string /* anonymous, use-credentials */,
  @optional
  cols: int,
  @optional
  colSpan: int,
  @optional
  content: string,
  @optional
  controls: bool,
  @optional
  coords: string /* set of values specifying the coordinates of a region */,
  @optional
  data: array<(string, string)>,
  @optional
  dateTime: string /* "valid date string with optional time" */,
  @optional
  default: bool,
  @optional
  defer: bool,
  @optional
  disabled: bool,
  @optional
  download: string /* should really be either a boolean, signifying presence, or a string */,
  @optional
  encType: string /* "application/x-www-form-urlencoded", "multipart/form-data" or "text/plain" */,
  @optional
  headers: string,
  @optional
  height: string,
  @optional
  max: string /* should be int or Js.Date.t */,
  @optional
  name: string,
  @optional
  placeholder: string,
  @optional
  src: string /* uri */,
  @optional @as("type")
  type_: string /* has a fixed but large-ish set of possible values */ /* use this one. Previous one is deprecated */,
  /* Form events */
  @optional @as("oninput")
  onInput: Dom.InputEvent.t => unit,
  @optional @as("onsubmit")
  onSubmit: Dom.UiEvent.t => unit,
  // mouse events
  @optional @as("onclick")
  onClick: Dom.MouseEvent.t => unit,
}

/* Called when instantiating regular tags like <a href=.../>

The extra code here is to allow passing data attributes as an array<(string, string)>
*/
let createDOMElementVariadic: (
  string,
  ~props: domProps=?,
  array<Mithril.element>,
) => Mithril.element = %raw(`function (name, attrs, children) {
  if (attrs && attrs.selector) {
    name = name + attrs.selector;
    delete attrs.selector;
  }
  if (attrs && attrs.data) {
    attrs.data.forEach((key_value) => {
      let [key, value] = key_value;
      attrs["data-" + key] = value;
    })
    delete attrs.data;
  }
  return mithril(name, attrs, children);
}`)
