%%raw(`import mithril from "mithril";`)

let null = Mithril.empty

/* Called when instantiating "Components" with non or single child, the child is passed as
 `children` in the attributes that's why this function extracts it from there */
let createElement = %raw(`function (name, attrs) {
  let children = undefined;
  if (attrs) {
    if (attrs.children) {
      children = attrs.children;
      delete attrs.children;
    }
  }
  return mithril(name, attrs, children);
}`)

/* Called when instantiating "Components" with many children */
@module("mithril") external createElementVariadic: ('a, 'b, 'c) => Mithril.element = "default"
