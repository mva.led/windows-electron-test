open Prelude

module WhoeverDef = {
  type t = {id: string, name: string}
  type index = [#id]
}

module WhoeverT = {
  include ReIndexed.MakeModel(WhoeverDef)

  let current: array<action> = [#filter(#all, user => user.name != "")]
  let clear: array<action> = [#clear]
}

module QueryDef = {
  let connection = Database.connection
  type request = {
    whoever: array<WhoeverT.action>
  }
  type response = {
    whoever: array<WhoeverT.t>
  }
  let make = (): request => {whoever: []}
  let makeResponse = (): response => {whoever: []}
}

include Database.MakeQuery(QueryDef)

module Whoever = {
  include WhoeverT

  /// Return a promise to get the current user data
  let getCurrentWhoever = _ => {
    {whoever: current}->do->then(result => resolve(result.whoever->Belt.Array.get(0)))
  }

  let setCurrentWhoever = user_name => {
    let uid = Prelude.uuid4()
    {whoever: [#clear, #save({id: uid, name: user_name})]}->do->then(r => resolve(r))
  }

}
