open Mithril
open Belt
open Prelude

type state = option<string>

let setWho = (vnode, _event) => {
  let value = vnode->Element.select("input")->Element.value
  let state = value->Option.flatMap(who =>
    switch who {
    | "" => None
    | name => Some(name)
    }
  )
  switch state {
    | Some(name) => Query.Whoever.setCurrentWhoever(name)->ignore
    | None => state->ignore
  }
  vnode->setState(state)
}

let make = _ =>
  component(None)
  ->oninit(vnode => {
    Query.Whoever.getCurrentWhoever()
    ->then(user => {
      vnode->setState(user->Option.map(user => user.name))
      resolve()
    })
    ->ignore
  })
  ->view(vnode => {
    let state = vnode->state
    let greetings =
      state->Option.mapWithDefault(<h1> {show("Hello, whoever you are!")} </h1>, who =>
        <h1> {show(`Hello, ${who}!`)} </h1>
      )

    <div> {show(greetings)} <input type_="text" onInput={setWho(vnode)} /> </div>
  })
