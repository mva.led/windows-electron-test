module DatabaseDef = {
  let migrations = () => {
    // Hey, you!
    //
    // Remember to add your new migrations at the end of this list.
    // Otherwise, they won't probably be applied unless you wipe the entire
    // application storage.
    [
      (db, _transaction) => {
        Js.log(("Running migration on DB", db))
        db->IDB.Migration.Database.createObjectStore("whoever")->ignore
      }
    ]
  }
}

include ReIndexed.MakeDatabase(DatabaseDef)
