import m from 'mithril';
import * as HelloWorld from './HelloWorld.bs';
import * as Database from './Database.bs';

const routes = {
  "/": HelloWorld.make
};

Database.connect("greetings").then(() => m.route(document.body, "/", routes));
