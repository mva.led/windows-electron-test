const esbuild = require('esbuild');
const chokidar = require('chokidar');
const isWatch = process.argv.includes('-w');
const isBuild = !isWatch;

const mode = isBuild ? 'production' : 'development';

var entryPoints = [
    'src/index.js'
];

const buildOptions = {
    entryPoints: entryPoints,
    define: {
        'process.env.NODE_ENV': JSON.stringify(mode),
        'process.env.API_HOST': JSON.stringify(process.env.API_HOST),
        'process.env.RELEASE_DATE': JSON.stringify(process.env.RELEASE_DATE),
        'process.env.RELEASE_HASH': JSON.stringify(process.env.RELEASE_HASH),
        'process.env.SENTRY_DSN': JSON.stringify(process.env.SENTRY_DSN),
        'process.env.SENTRY_ENVIRONMENT': JSON.stringify(process.env.SENTRY_ENVIRONMENT),
    },
    bundle: true,
    sourcemap: true,
    minify: false,
    incremental: isWatch,
    target: ['chrome90'],
    outfile: 'src/offline-dash.js',
    jsxFactory: 'm',
    loader: { '.js': 'jsx' },
};


const builder = esbuild.build(buildOptions);

if (isWatch) {
    builder.then((builder) => {
        chokidar.watch(['src/**/*.js'], { ignore: buildOptions.outfile, ignoreInitial: true }).on('all', (change, path) => {
            try {
                if (path !== "src/offline-dash.js") {
                    console.log(change, path);
                    builder.rebuild();
                }
            } catch (e) {
                console.error(`Build failed with ${e}... Ignoring and waiting`);
            }
        });
    });
}
