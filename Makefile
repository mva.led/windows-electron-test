PATH := ./node_modules/.bin/:$(PATH)

install: package.json
	yarn install

JS_FILES := $(shell find src/ -type f -name '*.js')
RES_FILES := $(shell find src/ -type f -name '*.res')

clean:
	rm -rf ./node_modules

compile-rescript: $(RES_FILES) install
	rescript build

compile-js: $(JS_FILES) install
	node esbuild.config.js

compile: $(RES_FILES) $(JS_FILES) install
	rescript build
	node esbuild.config.js

keep-compiling: $(JS_FILES) install
	node esbuild.config.js -w

run: install compile
	yarn start

.PHONY: keep-compiling run clean install compile-rescript compile-js
